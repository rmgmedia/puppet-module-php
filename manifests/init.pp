# Entry point class for installing PHP FPM, PHP CLI, PHP extensions and configuring all including PHP FPM process pools.
class php (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $version                              = 7.4,
  Boolean $manage_repos                       = true,
  Optional[Hash] $service                     = {
    enable => true,
    ensure => running,
  },
  Optional[Hash] $extensions                  = {},
  Optional[Hash] $fpm_pools                   = {},
  Optional[Hash] $settings                    = {},
  Optional[Hash] $cli_settings                = {},
) {

  if ($manage_repos) {
    class { 'php::repo':
      version => $version,
      # @TODO get prefix   prefix => $package_prefix,
      before  => Class['php::fpm'],
    }
    $package_require = [Class['php::repo']]
  } else {
    $package_require = []
  }

  class { 'php::fpm':
    ensure       => $ensure,
    version      => $version,
    service      => $service,
    settings     => $settings,
    cli_settings => $cli_settings,
  }

  if ($ensure != 'absent') {
    if (!empty($extensions)) {
      $extensions.each |String $name, Hash $extension| {
        php::extension { $name:
          * => merge({
            ensure      => $ensure,
            php_version => $version,
            require     => $package_require,
          }, $extension)
        }
      }
    }
  } else {
    case ($::facts['osfamily']) {
      'RedHat': {
        case ($version) {
          7.0: {
            $package_prefix = 'php70'
          }
          7.1: {
            $package_prefix = 'php71'
          }
          7.2: {
            $package_prefix = 'php72'
          }
          7.3: {
            $package_prefix = 'php73'
          }
          7.4: {
            $package_prefix = 'php74'
          }
          8.0: {
            $package_prefix = 'php80'
          }
          8.1: {
            $package_prefix = 'php81'
          }
          8.2: {
            $package_prefix = 'php82'
          }
          default: {
            fail("Unsupported PHP version ${version} for RedHat.")
          }
        }
        exec { "yum remove -y ${package_prefix}-*":
          command => "yum remove -y ${package_prefix}-*",
          onlyif  => "yum list installed | grep ${package_prefix}-",
        }
      }
      default: {
        fail("Unsupported osfamily: ${::facts['osfamily']}.")
      }
    }
  }

  if (!empty($fpm_pools)) {
    $fpm_pools.each |String $name, Hash $fpm_pool| {
      $fpm_pool_ensure = $ensure ? {
        'absent' => 'absent',
        default  => 'present',
      }
      php::fpm_pool { $name:
        * => merge({
          ensure => $fpm_pool_ensure,
        }, $fpm_pool)
      }
    }
  }

}
