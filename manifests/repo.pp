# Configures required repos to install PHP FPM packages
class php::repo (
  Float $version = 8.2,
) {

  # @TODO simplify this and use the remi-release from https://rpms.remirepo.net/enterprise/remi-release-7.rpm
  $releasever = $::operatingsystem ? {
    /(?i:Amazon)/ => '6',
    default       => '$releasever', # Yum var
  }
  case ($::facts['osfamily']) {
    'RedHat': {
      yumrepo { 'remi':
        descr      => 'Remi\'s PHP RPM Safe repository for Enterprise Linux $releasever - $basearch',
        mirrorlist => "http://rpms.remirepo.net/enterprise/${releasever}/remi/mirror",
        enabled    => 1,
        gpgcheck   => 1,
        gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
        priority   => 1,
      }

      case ($version) {

        7.0: {
          yumrepo { 'remi-php70':
            descr      => 'Remi\'s PHP 7.0 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php70/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        7.1: {
          yumrepo { 'remi-php71':
            descr      => 'Remi\'s PHP 7.1 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php71/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        7.2: {
          yumrepo { 'remi-php72':
            descr      => 'Remi\'s PHP 7.2 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php72/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        7.3: {
          yumrepo { 'remi-php73':
            descr      => 'Remi\'s PHP 7.3 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php73/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        7.4: {
          yumrepo { 'remi-php74':
            descr      => 'Remi\'s PHP 7.4 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php74/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        8.0: {
          yumrepo { 'remi-php80':
            descr      => 'Remi\'s PHP 8.0 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php80/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        8.1: {
          yumrepo { 'remi-php81':
            descr      => 'Remi\'s PHP 8.1 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php81/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        8.2: {
          yumrepo { 'remi-php82':
            descr      => 'Remi\'s PHP 8.2 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://cdn.remirepo.net/enterprise/${releasever}/php82/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        default: {
          fail("Unsupported PHP version ${version} for RedHat.")
        }
      }
    }
    default: {
      fail("Unsupported osfamily: ${::facts['osfamily']}.")
    }
  }
}
