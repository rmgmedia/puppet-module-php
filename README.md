robofirm/php Puppet Module
==========================

#### Table of Contents

1. [Module Description](#module-description)
2. [Setup - The basics of getting started with robofirm/php](#setup)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Module description

This module installs PHP FPM with extensions and provides the ability to configure php settings, php extensions, and 
FPM pools via hiera data. To reduce complexity, this module does not and will not support mod_php with Apache. PHP FPM 
is independent from the web server and can be used with Apache, Nginx, or other software. 

This module supports PHP 7.0, 7.1, 7.2, 7.3 and 7.4. This module also supports removal of a version that it has installed and 
installation of another version making it easy to upgrade existing servers.

## Setup

Add this to your profile:
```
include php
```

## Usage

All configuration is done via Hiera. Here is an example common configuration:
```yaml
php::version: 7.4
php::extensions:
  gd: {}
  ioncube-loader: {}
  intl: {}
  mbstring: {}
  mcrypt: {}
  mysql: {}
  pecl-zendopcache: {}
  pdo: {}
  posix: {}
  redis: {}
  soap: {}
  xml: {}
php::fpm_pools:
  www:
    settings:
      group: nginx
      catch_workers_output: "yes"
php::settings:
  Date:
    date.timezone: America/New_York
  PHP:
    expose_php: "off"
    post_max_size: 10M
    memory_limit: 512M
    realpath_cache_size: 32k
    realpath_cache_ttl: 7200
    upload_max_filesize: 10M
php::cli_settings:
  Date:
    date.timezone: America/New_York
  PHP:
    max_execution_time: 0
    memory_limit: 1024M
```

Note, the quotation marks around "off" and "yes". These are reserved words in Hiera and will automatically be converted
to boolean values if the quotes are omitted.

We highly recommend sticking to the Hiera method, but you can manually configure resources as well. The available 
resources are `php`, `php::fpm`, `php::fpm_pool`, and `php::extension`.

Resource signatures:
```puppet
class php (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $version                              = 7.4,
  Boolean $manage_repos                       = true,
  Optional[Hash] $service                     = {
    enable => true,
    ensure => running,
  },
  Optional[Hash] $extensions                  = { },
  Optional[Hash] $fpm_pools                   = { },
  Optional[Hash] $settings                    = { },
  Optional[Hash] $cli_settings                = { },
) {
```

```puppet
class php::fpm (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $version = 7.4,
  Hash $service = {
    enable => true,
    ensure => running,
  },
  Hash $settings = {},
  Hash $cli_settings = {},
) {}
```

```puppet
define php::fpm_pool (
  Enum['absent', 'present'] $ensure = 'present',
  Hash $settings = {},
) {}
```

```puppet
define php::extension (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $php_version = 7.4,
  Hash $settings = {},
  Hash $cli_settings = {},
) {}
```
 
### Configuration Reference

Note that while we will try our hardest to keep this reference up to date, there may be discrepencies. You can find 
these settings on your own by exploring the code starting from this entry point 
[manifests/init.pp](https://bitbucket.org/rmgmedia/puppet-module-php/src/master/manifests/init.pp).

#### `ensure` 

* Type: Enum["absent", "present", "latest"]
* Required: Yes
* Default: "present"

`present` installs PHP FPM

`absent` uninstalls PHP FPM and extensions

`latest` installs PHP FPM and updates it to the latest minor version within the given major version constraint

#### `version` 

* Type: Float
* Required: Yes
* Default: 7.4

The major PHP version to install. Current valid values are 7.0, 7.1, 7.2, 7.3, and 7.4 but will expand as new PHP versions are released.

#### `manage_repos`

* Type: Boolean
* Required: Yes
* Default: true

Whether or not to manage repos. If set to false, this module assumes that the repos have already been set up and depends
directly on the package names.

#### `extensions`

* Type: Hash
* Required: No
* Default: {}

A hash of extensions, each with these options:

##### `ensure`

* Type: Enum["absent", "present", "latest"]
* Required: Yes
* Default: "present"

`present` installs the extension

`absent` uninstalls the extension

`latest` installs the extension and updates it to the latest version for the given PHP FPM version

##### `settings`

* Type: Hash
* Required: No
* Default: {}

A hash of settings in ini format. These are used directly to modify the extension's ini file in php.d. The sections, keys,
and values will be specific to the extension. 

Here is an example for configuring Zend OpCache:

```yaml
php::extensions:
  pecl-zendopcache:
    settings:
      opcache.memory_consumption: 768
      opcache.max_accelerated_files: 60000
      opcache.interned_strings_buffer: 12
      opcache.save_comments: 0
      opcache.load_comments: 0
```

##### `cli_settings`

* Type: Hash
* Required: No
* Default: {}

A hash of settings in ini format as above, but for command line PHP.

#### `fpm_pools`

* Type: Hash
* Required: No
* Default: {}

##### `ensure`

* Type: Enum["absent", "present"]
* Required: Yes
* Default: "present"

`present` installs the pool

`absent` uninstalls the pool

##### `settings`

* Type: Hash
* Required: No
* Default: {}

A hash of settings in ini format. These are used directly to modify the pool's conf file in pool.d.

Example pool configuration:
```yaml
php::fpm_pools:
  www:
    settings:
      group: nginx
      catch_workers_output: "yes"
      pm: dynamic
      pm.max_children: 5
      pm.start_servers: 3
      pm.min_spare_servers: 2
      pm.max_spare_servers: 4
      pm.max_requests: 200
```
  
#### `settings`

* Type: Hash
* Required: No
* Default: {}

A hash of settings in ini format. These are used directly to modify the php.ini file. See the example configuration 
at the beginning of the [Usage](#usage) section for format.

Example:
```yaml
php::settings:
  Date:
    date.timezone: America/New_York
  PHP:
    expose_php: "off"
    max_execution_time: 18000
    max_input_time: 200
```
#### `cli_settings`

* Type: Hash
* Required: No
* Default: {}

A hash of settings in ini format as above, but for command line PHP.


## Reference

This module is intended to directly expose PHP FPM settings as much as possible via ini files, so that you can rely 
directly on PHP documentation. We don't want you to have to understand the inner workings of the module 
to use it.

## Limitations

OS Support is currently limited to CentOS or RedHat. There are no immediate plans to support other operating
systems as it would add complexity and doesn't fit our current needs. 

Remi repos are used and are configured by this module using the example42/yum repository manager module, unless 
manage_repos is set to false.

## Development

Open source is new to us at Robofirm, so this is TBD. Please raise issues in our 
[Bitbucket issue tracker](https://bitbucket.org/rmgmedia/puppet-module-php/issues).
