Robofirm PHP Puppet Module Changelog
====================================

## 2.1.0
- Added support for PHP 8.0, 8.1 and 8.2
- Updated to default to PHP 8.2

## 2.0.1
- Added ability to manage php fpm service
- Updated to default to PHP 7.4

## 2.0.0
- Removed deprecated yum module dependency
- Added php 7.3, 7.4 support
- Removed php 5.x support

## 1.1.2
- Fixed dependency issue with checking php-fpm service status

## 1.1.1
- Internal updates. Updated Bitbucket repo URLs in README and metadata.json 

## 1.1.0
- Added PHP 7.2 support

## 1.0.4
- Reverted to Hiera 4 syntax to continue to support Hiera 4.3 - 4.8.

## 1.0.3
- Updated requirements to Puppet >= 4.9.0 < 6.0.0
- Generalized OS support to version 7 for RedHat and CentOS
- Updated Readme to include ioncube and redis; commonly used php extensions

## 1.0.2
- Added epel repo include if set to manage repos. Some php extensions like mcrypt require the epel repo to be enabled 
  to install dependencies like libmcrypt. I decided not to depend on the libmcrypt package in this case because yum 
  already manages this dependency for the remi repo.

## 1.0.1
- Fixed PHP 7.0 and 7.1 lib, etc, and log dir symlinks
- Made removal of PHP remove all php extensions; Not just those listed in hiera data.
  Also addresses the issue of package removal dependency.

## 1.0.0
- Tagging this module as stable

## 0.2.7
- Added php::fpm_pools, php::settings, and php::cli_settings to hiera deep merge
- Ran all code through Puppet lint
- Added Puppet lint to Bitbucket Pipelines
- Fixed issue where using `php::ensure: latest` would cause some symlinks to be removed

## 0.2.6
- Added timezone setting for cli in example configuration

## 0.2.5
- Removed boilerplate setup instructions
- Cleaned up README
- Added MIT license
- Added humans.txt

## 0.2.4
- Fixed changelog

## 0.2.3
- Updated readme

## 0.2.2
- Updated to correctly restart php-fpm service when needed
- Updated to remove all php extensions when removing php
- Updated to remove symlinks when removing php
- Updated to only make symlinks if they are not already the expected path
- Improved handling of custom group in combination with writable paths for error log, session save, and wsdlcache

## 0.2.1
- Updated metadata.json
- Expanded documentation for public consumption

## 0.2.0
- Fixed issues with installing PHP 5.5 and 5.6 on RedHat
- Made behavior when removing PHP more complete
